# Docker + Vite
https://youtu.be/3xDAU5cvi5E


```bat
docker run -d --rm --name vite-docker -p 3000:3000 -v %cd%\src:/app/src -e CHOKIDAR_USEPOLLING=true slimbde/vite-docker
```
### This stands for: 
***
```ps
-d      detached  
--rm    remove container when stopped  
--name  container name  
-p      port forwarding, left is local port:right is remote port
-v      volume assertion, left is local path:right is docker path
-e      environment variable `CHOKIDAR_USEPOLLING=true` is necessary to make volume work
```

### Docker-compose
```ps
docker-compose up -d
docker-compose down
```
starts all the services, creates a default network