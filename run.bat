@echo off

docker run -d --rm --name vite-docker -p 3000:3000 -v %cd%\src:/app/src -e CHOKIDAR_USEPOLLING=true slimbde/vite-docker
